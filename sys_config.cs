﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping
{
    public static class sys_config
    {
        public static Crop DefaultCrop
        {
            get
            {
                return BusinessLayerService.CropBLL().GetDefaultCrop();
            }
        }

        public static string CurrentUser
        {
            get
            {
                return Environment.UserDomainName + "\\" + Environment.UserName;
            }
        }
    }
}
