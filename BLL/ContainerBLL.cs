﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMShipping.DomainModel;
using AOMShipping.DAL;
using DAL;

namespace AOMShipping.BLL
{
    public interface IContainerBLL
    {
        void Add(short crop, string containerNo);
        void Update(ContainerDetail model);
        void Delete(Guid containerID);
        List<ContainerDetail> GetByCrop(short crop);
        ContainerDetail GetSingle(Guid containerID);
    }

    public class ContainerBLL : IContainerBLL
    {
        UnitOfWork _uow;
        public ContainerBLL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(short crop, string containerNo)
        {
            try
            {
                if (containerNo == null || containerNo == "")
                    throw new ArgumentNullException("Container Number not found.");

                var model = _uow.ContainerDetailRepository.GetSingle(s => s.ContainerNo == containerNo);
                if (model != null)
                    throw new ArgumentException("Duplicate a container number");

                //model.ContainerNo = containerNo;
                //model.CreateBy = Environment.UserName;
                //model.CreateDate = DateTime.Now;
                //model.ModifiedBy = Environment.UserName;
                //model.ModifiedDate = DateTime.Now;

                _uow.ContainerDetailRepository.Add(new ContainerDetail
                {
                    Crop = crop,
                    ContainerID = Guid.NewGuid(),
                    ContainerNo = containerNo,
                    CreateBy = Environment.UserName,
                    CreateDate = DateTime.Now,
                    ModifiedBy = Environment.UserName,
                    ModifiedDate = DateTime.Now
                });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ContainerDetail model)
        {
            try
            {
                if (model.ContainerNo == null || model.ContainerNo == "")
                    throw new ArgumentNullException("Container Number not found.");

                var db = _uow.ContainerDetailRepository.GetSingle(s => s.ContainerNo == model.ContainerNo);
                if (db != null)
                    throw new ArgumentException("This container number is dupplicated");

                model.ModifiedBy = Environment.UserName;
                model.ModifiedDate = DateTime.Now;

                _uow.ContainerDetailRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid containerID)
        {
            try
            {
                if (containerID == null)
                    throw new ArgumentNullException("Container ID is null.");

                var model = _uow.ContainerDetailRepository.GetSingle(c => c.ContainerID == containerID);

                if (model == null)
                    throw new ArgumentNullException("Find a data not found!");

                if (model.Shippings.Count > 0)
                    throw new ArgumentException("This container have to many bales.The system cannot delete.");

                _uow.ContainerDetailRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ContainerDetail GetSingle(Guid containerID)
        {
            return _uow.ContainerDetailRepository.GetSingle(c => c.ContainerID == containerID);
        }
        public List<ContainerDetail> GetByCrop(short crop)
        {
            return _uow.ContainerDetailRepository.Get(c => c.Crop == crop).ToList();
        }
    }
}
