﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using AOMShipping.DomainModel;
using Spire.Xls;
using DAL;

namespace AOMShipping.BLL
{
    public interface IShippingBLL
    {
        void Add(Shipping shipping);
        void Update(Shipping shipping);
        void ScanToContainer(string baleBarcode, Guid containerID);
        void RemoveFormContainer(string baleBarcode);
        void Delete(Shipping shipping);
        Shipping GetByBaleBarcode(string baleBarcode);
        Shipping GetByBaleNumber(int baleNumber);
        List<Shipping> GetByShippingCode(string shippingCode);
        List<Shipping> GetByCrop(short crop);
        List<Shipping> GetByContainerID(Guid containerID);
        List<Shipping> ExcelTransform(string FilePath);
        List<Shipping> BaleBarcodeEmpty(List<Shipping> collections);
        List<Shipping> DataTableToList(DataTable Datable);
    }

    public class ShippingBLL : IShippingBLL
    {
        UnitOfWork _uow;

        public ShippingBLL()
        {
            _uow = new UnitOfWork();
        }
        public void Add(Shipping shipping)
        {
            try
            {
                if (shipping == null)
                    throw new ArgumentNullException("Shipping data not found.");

                _uow.ShippingRepository.Add(shipping);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(Shipping shipping)
        {
            try
            {
                if (shipping.BaleBarcode == null || shipping.BaleBarcode == "")
                    throw new ArgumentException("BaleBarcode not found.");

                _uow.ShippingRepository.Update(shipping);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void ScanToContainer(string baleBarcode, Guid containerID)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("Please input a bale barcode.");

                if (containerID == null)
                    throw new ArgumentNullException("Please select a container.");

                var shipping = GetByBaleBarcode(baleBarcode);
                if (shipping == null)
                    throw new ArgumentNullException("Find a data not found!");

                shipping.ContainerID = containerID;
                shipping.ToContainerDate = DateTime.Now;
                shipping.ToContainerUser = Environment.UserName;

                //currentShipping.ContainerDetail = null;
                //currentShipping.ContainerID = null;

                //using (AOMShippingSystemEntities context = new AOMShippingSystemEntities())
                //{
                //    context.sp_UpdateShippingContainerID(shipping.BaleBarcode, shipping.ContainerID, Environment.UserName);
                //}

                _uow.ShippingRepository.Update(shipping);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RemoveFormContainer(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentNullException("Please input a bale barcode.");

                var shipping = GetByBaleBarcode(baleBarcode);
                if (shipping == null)
                    throw new ArgumentNullException("Find a data not found!");

                //Set temp data for adding after delete.
                //Shipping temp = new Shipping();

                //temp = shipping;
                //temp.ArrivedDate = null;
                //temp.ContainerDetail = null;
                //temp.ContainerID = null;
                //temp.CheckerUser = null;

                //ShippingService._shippingBLL().DeleteShippingData(shipping);
                //ShippingService._shippingBLL().AddShippingData(temp);

                shipping.ToContainerDate = null;
                shipping.ToContainerUser = null;
                shipping.ContainerDetail = null;
                shipping.ContainerID = null;

                _uow.ShippingRepository.Update(shipping);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Shipping shipping)
        {
            try
            {
                if (GetByBaleBarcode(shipping.BaleBarcode) == null)
                    throw new ArgumentNullException("Find data not found!");

                _uow.ShippingRepository.Remove(shipping);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Shipping GetByBaleBarcode(string baleBarcode)
        {
            return _uow.ShippingRepository.GetSingle(s => s.BaleBarcode == baleBarcode,
                    s => s.ContainerDetail);
        }
        public Shipping GetByBaleNumber(int baleNumber)
        {
            return _uow.ShippingRepository.GetSingle(s => s.BaleNumber == baleNumber,
                    s => s.ContainerDetail);
        }
        public List<Shipping> GetByCrop(short crop)
        {
            return _uow.ShippingRepository.Get(s => s.Crop == crop,
                null, s => s.ContainerDetail).ToList();
        }
        public List<Shipping> GetByShippingCode(string shippingCode)
        {
            return _uow.ShippingRepository.Get(s => s.ShippingCode == shippingCode,
                    null, s => s.ContainerDetail).ToList();
        }
        public List<Shipping> GetByContainerID(Guid containerID)
        {
            return _uow.ShippingRepository
                .Get(s => s.ContainerID == containerID, null, s => s.ContainerDetail)
                .ToList();
        }
        public List<Shipping> BaleBarcodeEmpty(List<Shipping> collections)
        {
            try
            {
                return collections.Where(w => w.BaleBarcode == string.Empty).ToList(); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Shipping> ExcelTransform(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }
        public List<Shipping> DataTableToList(DataTable Datable)
        {
            List<Shipping> list = new List<Shipping>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row["BaleBarcode"].ToString() == "")
                    break;

                list.Add(new Shipping
                {
                    BaleBarcode = row["BaleBarcode"].ToString(),
                    BaleNumber = Convert.ToInt32(row["BaleNumber"]),
                    Crop = Convert.ToInt16(row["Crop"].ToString()),
                    FarmerID = row["FarmerID"].ToString(),
                    DocumentNumber = Convert.ToInt16(row["DocumentNumber"].ToString()),
                    BuyingDocumentCode = row["BuyingDocumentCode"].ToString(),
                    ProjectTypeID = row["ProjectTypeID"].ToString(),
                    RegisterBarcodeDate = DateTime.Parse(row["RegisterBarcodeDate"].ToString()),
                    BuyingGradeCrop = Convert.ToInt16(row["BuyingGradeCrop"].ToString()),
                    BuyingGrade = row["BuyingGrade"].ToString(),
                    BuyingGradeRecordDate = DateTime.Parse(row["BuyingGradeRecordDate"].ToString()),
                    BuyingWeight = Convert.ToDecimal(row["BuyingWeight"].ToString()),
                    BuyingWeightRecordDate = DateTime.Parse(row["BuyingWeightRecordDate"].ToString()),
                    LoadBaleToTruckDate = DateTime.Parse(row["LoadBaleToTruckDate"].ToString()),
                    UnitPrice = Convert.ToDecimal(row["UnitPrice"].ToString()),
                    TransportationCode = row["TransportationCode"].ToString(),
                    TransportationTruckNo = row["TransportationTruckNo"].ToString(),
                    ReceivingWeight = Convert.ToDecimal(row["ReceivingWeight"].ToString()),
                    ReceivedDate = DateTime.Parse(row["ImportDate"].ToString()),
                    ReceivedBy = row["ReceivedBy"].ToString(),
                    ShippingCode = row["ShippingCode"].ToString(),
                    ShippingToTruckDate = DateTime.Parse(row["ShippingToTruckDate"].ToString()),
                    ImportBy = null,
                    ImportDate = null,
                    ContainerID = null,
                    ToContainerDate = null,
                    ToContainerUser = null,
                });
            }
            return list;
        }
    }
}
