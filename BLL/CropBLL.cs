﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMShipping.DomainModel;
using AOMShipping.BLL;
using DAL;

namespace AOMShipping.BLL
{
    public interface ICropBLL
    {
        Crop GetDefaultCrop();
    }

    public class CropBLL : ICropBLL
    {
        UnitOfWork _uow;
        public CropBLL()
        {
            _uow = new UnitOfWork();
        }

        public Crop GetDefaultCrop()
        {
            return _uow.CropRepository.GetSingle(c => c.DefaultStatus == true);
        }
    }
}
