﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping.BLL
{
    public class BusinessLayerService
    {

        public static IShippingBLL ShippingBLL()
        {
            IShippingBLL bll = new ShippingBLL();
            return bll;
        }

        public static IContainerBLL ContainerDetailBLL()
        {
            IContainerBLL bll = new ContainerBLL();
            return bll;
        }

        public static ICropBLL CropBLL()
        {
            ICropBLL bll = new CropBLL();
            return bll;
        }
    }
}
