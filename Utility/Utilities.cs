﻿using AOMShipping.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;

namespace AOMShipping.Utility
{
    public static class Utilities
    {
        public static string ExportContainerDetail(Guid containerID, string folderName)
        {
            string name = @"" + folderName + "\\Container-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";
            FileInfo info = new FileInfo(name);

            List<vm_Shipping> list = new List<vm_Shipping>();
            list = AOMShipping.Helper.ShippingHelper.GetByContainerID(containerID);

            if (info.Exists)
                File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Container-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day);
                ws.Cells["A1"].LoadFromCollection(list, true);
                ws.Protection.IsProtected = false;
                using (ExcelRange col = ws.Cells[2, 6, 2 + list.Count, 6]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 9, 2 + list.Count, 9]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 11, 2 + list.Count, 11]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 12, 2 + list.Count, 12]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 14, 2 + list.Count, 14]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 17, 2 + list.Count, 17]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 20, 2 + list.Count, 20]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }
        
        public static System.Windows.Forms.OpenFileDialog InputFile(System.Windows.Forms.OpenFileDialog choofdlog,
            string Filter,int FilterIndex,bool Multiselect)
        {
            //Show file select dialog and get file name and location.
            choofdlog.Filter = Filter;
            choofdlog.FilterIndex = FilterIndex;
            choofdlog.Multiselect = Multiselect;
            return choofdlog;
        }
    }
}
