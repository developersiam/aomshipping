﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping.Utility
{
    class TSCPrinter
    {
        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "about")]
        public static extern int about();

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "openport")]
        public static extern int openport(string printername);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "barcode")]
        public static extern int barcode(string x, string y, string type,
                    string height, string readable, string rotation,
                    string narrow, string wide, string code);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "clearbuffer")]
        public static extern int clearbuffer();

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "closeport")]
        public static extern int closeport();

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "downloadpcx")]
        public static extern int downloadpcx(string filename, string image_name);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "formfeed")]
        public static extern int formfeed();

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "nobackfeed")]
        public static extern int nobackfeed();

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "printerfont")]
        public static extern int printerfont(string x, string y, string fonttype,
                        string rotation, string xmul, string ymul,
                        string text);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "printlabel")]
        public static extern int printlabel(string set, string copy);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "sendcommand")]
        public static extern int sendcommand(string printercommand);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "setup")]
        public static extern int setup(string width, string height,
                  string speed, string density,
                  string sensor, string vertical,
                  string offset);

        [DllImport("C:\\windows\\system\\TSCLIB.dll", EntryPoint = "windowsfont")]
        public static extern int windowsfont(int x, int y, int fontheight,
                        int rotation, int fontstyle, int fontunderline,
                        string szFaceName, string content);
    }
}
