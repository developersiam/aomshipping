﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMShipping.BLL;
using AOMShipping.DomainModel;
using AOMShipping.Utility;
using System.Drawing.Printing;

namespace AOMInventory.Utility
{
    public class PrintBarcode
    {
        public void PrintBarcodeSticker(int baleNumber)
        {
            Shipping shipping = new Shipping();
            shipping = BusinessLayerService.ShippingBLL().GetByBaleNumber(baleNumber);

            if (shipping == null) throw new ArgumentException("Find data not found!");

            PrinterSettings setting = new PrinterSettings();
            TSCPrinter.openport(setting.PrinterName);
            TSCPrinter.setup("103", "76", "2.0", "1", "0", "0", "0");
            TSCPrinter.sendcommand("GAP  3 mm,0");
            TSCPrinter.sendcommand("DIRECTION 1");
            TSCPrinter.clearbuffer();

            TSCPrinter.sendcommand("BAR 40,40,4,528");          //เส้นตั้ง หน้าสุด
            TSCPrinter.sendcommand("BAR 776,40,4,528");          //เส้นตั้ง หลังสุด
            TSCPrinter.sendcommand("BAR 40,40,736,4");          //เส้นนอน บนสุด
            TSCPrinter.sendcommand("BAR 40,568,736,4");          //เส้นนอน ล่างสุด

            TSCPrinter.barcode("80", "80", "128", "80", "0", "0", "2", "2", shipping.BaleBarcode);
            TSCPrinter.windowsfont(80, 160, 40, 0, 0, 0, "arial", shipping.BaleBarcode);

            TSCPrinter.barcode("664", "552", "128", "80", "0", "270", "2", "2", shipping.BaleBarcode);
            TSCPrinter.windowsfont(624, 520, 40, 90, 0, 0, "arial", shipping.BaleBarcode);

            TSCPrinter.windowsfont(80, 200, 80, 0, 0, 0, "arial", "ID : " + shipping.FarmerID);
            TSCPrinter.windowsfont(80, 280, 40, 0, 0, 0, "arial", "Name : " + "-");

            TSCPrinter.windowsfont(80, 360, 40, 0, 0, 0, "arial", "CY : " + shipping.Crop);
            TSCPrinter.windowsfont(80, 400, 40, 0, 0, 0, "arial", "Type : " + shipping.ProjectTypeID);
            TSCPrinter.windowsfont(80, 440, 40, 0, 0, 0, "arial", "Bale No : " + shipping.BaleNumber);

            TSCPrinter.windowsfont(80, 480, 40, 0, 0, 0, "arial", "Regis Date : " 
                + shipping.RegisterBarcodeDate.ToShortDateString() 
                + " " + shipping.RegisterBarcodeDate.ToLongTimeString());
            
            TSCPrinter.printlabel("1", "1");
            TSCPrinter.closeport();
        }
    }
}
