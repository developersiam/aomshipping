﻿using AOMShipping.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        IGenericDataRepository<Crop> CropRepository { get; }
        IGenericDataRepository<ContainerDetail> ContainerDetailRepository { get; }
        IGenericDataRepository<Shipping> ShippingRepository { get; }
        void Save();
    }
}
