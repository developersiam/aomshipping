﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DAL
{
    public class GenericDataRepository<T> : IGenericDataRepository<T> where T : class
    {
        private DbContext context;
        private DbSet<T> dbSet;

        public GenericDataRepository(DbContext context)
        {
            this.context = context;
            this.dbSet = context.Set<T>();
        }

        List<T> IGenericDataRepository<T>.Get(Expression<Func<T, bool>> filter, Func<IQueryable<T>, 
            IOrderedQueryable<T>> orderBy, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = dbSet;
            foreach (Expression<Func<T, object>> include in includes)
                query = query.Include(include);

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query.ToList();
        }

        public virtual IQueryable<T> Query(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>,
            IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] include)
        {
            IQueryable<T> query = dbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                query = orderBy(query);

            return query;
        }

        public virtual T GetSingle(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes)
        {
            T item = null;
            IQueryable<T> dbQuery = context.Set<T>().Where(where);

            foreach (Expression<Func<T, object>> include in includes)
                dbQuery = dbQuery.Include<T, object>(include);

            item = dbQuery
                .AsNoTracking()
                .FirstOrDefault(where);
            return item;
        }

        public void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            dbSet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Remove(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                dbSet.Attach(entity);

            dbSet.Remove(entity);
        }
    }
}
