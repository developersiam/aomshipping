﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AOMShipping.DomainModel;
using AOMShipping.DAL;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly AOMShippingSystemEntities _context;
        private IGenericDataRepository<Crop> _cropRepository;
        private IGenericDataRepository<ContainerDetail> _containerDetailRepository;
        private IGenericDataRepository<Shipping> _shippingRepository;

        public UnitOfWork()
        {
            _context = new AOMShippingSystemEntities();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        public IGenericDataRepository<Crop> CropRepository
        {
            get
            {
                return _cropRepository ?? (_cropRepository = new GenericDataRepository<Crop>(_context));
            }
        }

        public IGenericDataRepository<ContainerDetail> ContainerDetailRepository
        {
            get
            {
                return _containerDetailRepository ?? (_containerDetailRepository = 
                    new GenericDataRepository<ContainerDetail>(_context));
            }
        }

        public IGenericDataRepository<Shipping> ShippingRepository
        {
            get
            {
                return _shippingRepository ?? (_shippingRepository = new GenericDataRepository<Shipping>(_context));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}