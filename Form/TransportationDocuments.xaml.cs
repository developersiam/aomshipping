﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using AOMShipping.ViewModel;
using AOMShipping.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for ImportShippingDocumentPage.xaml
    /// </summary>
    public partial class TransportationDocuments : Page
    {
        List<vm_TruckPackingList> _truckPackingList;
        public TransportationDocuments()
        {
            InitializeComponent();

            _truckPackingList = new List<vm_TruckPackingList>();

            ReloadPage();
        }

        private void ReloadPage()
        {
            try
            {
                _truckPackingList = Helper.ShippingDocumentHelper.GetByCrop(sys_config.DefaultCrop.Crop1);

                transportationDocumentDataGrid.ItemsSource = null;
                transportationDocumentDataGrid.ItemsSource = _truckPackingList.OrderByDescending(x=>x.ShippingToTruckDate);

                transportLabel.Content = _truckPackingList.Count.ToString("N0");
                importedBaleLabel.Content = _truckPackingList.Sum(x => x.TotalImported).ToString("N0");
                shippedBaleLabel.Content = _truckPackingList.Sum(x => x.TotalShipped).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void viewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDocumentDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (vm_TruckPackingList)transportationDocumentDataGrid.SelectedItem;
                ShippingDocumentDetail window = new ShippingDocumentDetail(BusinessLayerService.ShippingBLL()
                    .GetByShippingCode(selectedItem.ShippingCode));
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void truckPackingListButton_Click(object sender, RoutedEventArgs e)
        {
            TruckPackingLists window = new TruckPackingLists();
            window.ShowDialog();
            ReloadPage();
        }
    }
}
