﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using AOMShipping.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for SetupContainerPage.xaml
    /// </summary>
    public partial class SetupContainer : Page
    {
        Crop _crop;
        ContainerDetail _container;
        List<vm_Container> _containerList;

        public SetupContainer()
        {
            InitializeComponent();

            _crop = new Crop();
            _crop = BusinessLayerService.CropBLL().GetDefaultCrop();
            _container = new ContainerDetail();
            _containerList = new List<vm_Container>();
        }

        private void ReloadContainerList()
        {
            try
            {
                _containerList = Helper.ContainerDetailHelper.GetByCrop(_crop.Crop1);
                ContainerDataGrid.ItemsSource = null;
                ContainerDataGrid.ItemsSource = _containerList.OrderBy(c => c.ContainerNo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearForm()
        {
            ContainerNoTextBox.Text = "";
            ContainerNoTextBox.Focus();
            SaveButton.Content = "Save";
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadContainerList();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerNoTextBox.Text == "")
                {
                    MessageBox.Show("Please input container number.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ContainerNoTextBox.Focus();
                    return;
                }
                if (SaveButton.Content.ToString() == "Save")
                {
                    BusinessLayerService.ContainerDetailBLL().Add(sys_config.DefaultCrop.Crop1, ContainerNoTextBox.Text.Replace(" ", string.Empty));
                }
                else
                {
                    _container.ContainerNo = ContainerNoTextBox.Text;
                    BusinessLayerService.ContainerDetailBLL().Update(_container);
                }
                ClearForm();
                ReloadContainerList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadContainerList();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (vm_Container)ContainerDataGrid.SelectedItem;
                _container = BusinessLayerService.ContainerDetailBLL().GetSingle(selectedItem.ContainerID);
                ContainerNoTextBox.Text = _container.ContainerNo;
                SaveButton.Content = "Edit";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerDataGrid.SelectedIndex < 0)
                    return;
                if (MessageBox.Show("Do you want to delete this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (ContainerDetail)ContainerDataGrid.SelectedItem;
                BusinessLayerService.ContainerDetailBLL().Delete(selectedItem.ContainerID);

                ClearForm();
                ReloadContainerList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
