﻿using AOMInventory.Utility;
using AOMShipping.BLL;
using AOMShipping.DomainModel;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for RePrintBarcodePage.xaml
    /// </summary>
    public partial class RePrintBarcodePage : Page
    {
        Shipping _shipping;

        public RePrintBarcodePage()
        {
            InitializeComponent();
            _shipping = new Shipping();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DisplayBaleInfo()
        {
            try
            {
                BaleBarcodeTextBox.Text = _shipping.BaleBarcode;
                BaleNumberTextBox.Text = _shipping.BaleNumber.ToString();
                FarmerIDTextBox.Text = _shipping.FarmerID;
                ProjectTypeTextBox.Text = _shipping.ProjectTypeID;
                BuyingGradeTextBox.Text = _shipping.BuyingGrade;
                BuyingWeightTextBox.Text = string.Format("{0:N2}", _shipping.BuyingWeight);
                TransportationCodeTextBox.Text = _shipping.TransportationCode;
                ContainerNoTextBox.Text = _shipping.ContainerDetail != null ? _shipping.ContainerDetail.ContainerNo : "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DisplayBaleBarcodeInfo(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                _shipping = BusinessLayerService.ShippingBLL().GetByBaleBarcode(baleBarcode);
                if (_shipping == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DisplayBaleBarcodeInfoByBaleNumber(short baleNumber)
        {
            try
            {
                _shipping = BusinessLayerService.ShippingBLL().GetByBaleNumber(baleNumber);
                if (_shipping == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleNumberTextBox.Text = "";
                    BaleNumberTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Text = "";
                BaleNumberTextBox.Text = "";
                FarmerIDTextBox.Text = "";
                ProjectTypeTextBox.Text = "";
                BuyingGradeTextBox.Text = "";
                BuyingWeightTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PrintBarcode _printBarcode = new PrintBarcode();
                _printBarcode.PrintBarcodeSticker(Convert.ToInt16(BaleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfo(BaleBarcodeTextBox.Text.Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfoByBaleNumber(Convert.ToInt16(BaleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
