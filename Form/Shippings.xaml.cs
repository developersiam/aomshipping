﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using AOMShipping.BLL;
using AOMShipping.DomainModel;
using System.Windows.Input;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for ScanBaleToContainer.xaml
    /// </summary>
    public partial class Shippings : Window
    {
        Shipping _shipping;

        public Shippings()
        {
            try
            {
                InitializeComponent();

                _shipping = new Shipping();

                ReloadContainerList();
                ToContainerRadioButton.IsChecked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Save(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (ContainerComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a container no.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ContainerComboBox.Focus();
                    BaleNumberTextBox.Text = "";
                    BaleBarcodeTextBox.Text = "";
                    return;
                }

                if (ToContainerRadioButton.IsChecked == true)
                {
                    var selectedItem = (ContainerDetail)ContainerComboBox.SelectedItem;

                    /// Difference a container. So,show confirmation to move the bale.
                    /// 
                    if (_shipping.ContainerID != null && selectedItem.ContainerID != _shipping.ContainerID)
                    {
                        if (MessageBox.Show("This is a differnce container. Do you want to move the bale.",
                            "warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                            return;
                    }

                    BusinessLayerService.ShippingBLL().ScanToContainer(baleBarcode,
                        Guid.Parse(ContainerComboBox.SelectedValue.ToString()));
                }
                else
                {
                    var selectedItem = (ContainerDetail)ContainerComboBox.SelectedItem;

                    if(_shipping.ContainerID == null)
                    {
                        MessageBox.Show("This bale have not to a container. Cannot remove this.", "warning", 
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    /// Difference a container. So,show confirmation to move the bale.
                    /// 
                    if (selectedItem.ContainerID != _shipping.ContainerID)
                    {
                        MessageBox.Show("This is a differnce container. The system cannot remove." + Environment.NewLine +
                            "This bale from container no " + _shipping.ContainerDetail.ContainerNo + ".",
                            "warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        return;
                    }

                    BusinessLayerService.ShippingBLL().RemoveFormContainer(baleBarcode);
                }

                ReloadContainerDetail();
                BaleBarcodeTextBox.Text = "";
                BaleNumberTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ReloadContainerList()
        {
            try
            {
                ContainerComboBox.ItemsSource = null;
                ContainerComboBox.ItemsSource = BusinessLayerService.ContainerDetailBLL()
                    .GetByCrop(sys_config.DefaultCrop.Crop1)
                    .OrderBy(c => c.ContainerNo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ReloadContainerDetail()
        {
            try
            {
                if (ContainerComboBox.SelectedIndex < 0)
                    return;

                var selectedItem = (ContainerDetail)ContainerComboBox.SelectedItem;

                ContainerDetailDataGrid.ItemsSource = null;
                ContainerDetailDataGrid.ItemsSource = BusinessLayerService.ShippingBLL()
                    .GetByContainerID(selectedItem.ContainerID)
                    .OrderByDescending(c => c.ToContainerDate);

                totalItemLabel.Content = string.Format("{0:N0}", ContainerDetailDataGrid.Items.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ContainerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReloadContainerDetail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RefreshButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                ReloadContainerDetail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.Clear();
                    return;
                }

                _shipping = BusinessLayerService.ShippingBLL()
                    .GetByBaleBarcode(BaleBarcodeTextBox.Text
                    .Replace(" ", string.Empty));

                if (_shipping == null)
                {
                    MessageBox.Show("Find a data not found!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.Clear();
                    BaleNumberTextBox.Clear();
                    return;
                }
                BaleNumberTextBox.Text = string.Format("{0:N0}", _shipping.BaleNumber);
                Save(_shipping.BaleBarcode);
                BaleBarcodeTextBox.Clear();
                BaleNumberTextBox.Clear();
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BaleNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale number.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleNumberTextBox.Focus();
                    BaleNumberTextBox.Clear();
                    return;
                }

                _shipping = BusinessLayerService.ShippingBLL()
                    .GetByBaleNumber(Convert.ToInt32(BaleNumberTextBox.Text.Replace(" ", string.Empty)));

                if (_shipping == null)
                {
                    MessageBox.Show("Find a data not found!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleNumberTextBox.Clear();
                    BaleBarcodeTextBox.Clear();
                    return;
                }

                BaleBarcodeTextBox.Text = _shipping.BaleBarcode;
                Save(_shipping.BaleBarcode);
                BaleNumberTextBox.Clear();
                BaleBarcodeTextBox.Clear();
                BaleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TakeOffRadioButton_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            ToContainerRadioButton.IsChecked = false;
        }

        private void ToContainerRadioButton_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            TakeOffRadioButton.IsChecked = false;
        }

        private void TakeOffButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerDetailDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you wnat to take off this bale form a container?", "warning!", 
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (Shipping)ContainerDetailDataGrid.SelectedItem;
                TakeOffRadioButton.IsChecked = true;

                BusinessLayerService.ShippingBLL().RemoveFormContainer(selectedItem.BaleBarcode);
                ReloadContainerDetail();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a container no.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ContainerComboBox.Focus();
                    return;
                }

                var selectedItem = (ContainerDetail)ContainerComboBox.SelectedItem;
                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;
                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = Utility.Utilities.ExportContainerDetail(selectedItem.ContainerID, folderDlg.SelectedPath);
                    Environment.SpecialFolder root = folderDlg.RootFolder;
                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PackingListPreviewButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ContainerComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a container no.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ContainerComboBox.Focus();
                    return;
                }
                var selectedItem = (ContainerDetail)ContainerComboBox.SelectedItem;
                Report.RPTSHP002 window = new Report.RPTSHP002(selectedItem.ContainerID);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BaleBarcodeTextBox.Focus();
        }
    }
}
