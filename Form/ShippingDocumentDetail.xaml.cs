﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for ShippingDocumentDetail.xaml
    /// </summary>
    public partial class ShippingDocumentDetail : Window
    {
        List<Shipping> _transportationList;

        public ShippingDocumentDetail(List<Shipping> _list)
        {
            try
            {
                InitializeComponent();

                _transportationList = new List<Shipping>();
                _transportationList = _list;

                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ReloadPage()
        {
            try
            {
                int totalToContainer = 0;

                // เป็นการปรับค่าของ _list ที่ได้จากการอ่านข้อมูลจาก excel เพื่อนำไปแสดงบนหน้าจอว่า... ยาห่อใดบ้างที่เคยมีการ import ไปก่อนนี้แล้ว
                List<Shipping> dbList = new List<Shipping>();
                dbList = BusinessLayerService.ShippingBLL().GetByCrop(_transportationList.FirstOrDefault().Crop);

                if (dbList.Count > 0)
                {
                    foreach (var item in _transportationList)
                    {
                        var fromDB = dbList.Single(l => l.BaleBarcode == item.BaleBarcode);
                        if (fromDB == null)
                            return;
                        if (fromDB.ToContainerUser != null)
                        {
                            item.ToContainerUser = "To Container";
                            item.ToContainerDate = fromDB.ToContainerDate;
                            totalToContainer++;
                        }
                        else
                        {
                            item.ToContainerUser = "Wait...";
                        }
                    }
                }

                transportationDetailDataGrid.ItemsSource = null;
                transportationDetailDataGrid.ItemsSource = _transportationList.OrderBy(t => t.ImportBy);
                transportationCodeTextBox.Text = _transportationList.FirstOrDefault().ShippingCode;
                waitForShippingTextBox.Text = string.Format("{0:N0}", _transportationList.Count - totalToContainer);
                toContainerTextBox.Text = string.Format("{0:N0}", totalToContainer);
                totalBalesTextBox.Text = string.Format("{0:N0}", _transportationList.Count);

                var list = _transportationList.GroupBy(x => x.BuyingGrade).Select(x => new Shipping
                {
                    BuyingGrade = x.Key,
                    BaleNumber = x.Count(), /// Representation for total bales from transportation code.
                    DocumentNumber = Convert.ToInt16(x.Where(y => y.ContainerID != null).Count()), /// Representation for total bales on the container.
                    BuyingWeight = x.Sum(y => y.BuyingWeight), /// Representation for total wait from transportation code.
                    ReceivingWeight = x.Where(y => y.ContainerID != null).Sum(y => y.BuyingWeight) /// Representation for total weight on the container.
                });

                summaryByGradeDataGrid.ItemsSource = null;
                summaryByGradeDataGrid.ItemsSource = list.OrderBy(x => x.BuyingGrade);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
