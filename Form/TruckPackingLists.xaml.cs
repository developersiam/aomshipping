﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace AOMShipping.Form
{
    /// <summary>
    /// Interaction logic for TransportationDocumentDetailWindow.xaml
    /// </summary>
    public partial class TruckPackingLists : Window
    {
        List<Shipping> _truckPackingList;
        List<Shipping> _importedList;

        public TruckPackingLists()
        {
            try
            {
                InitializeComponent();

                _truckPackingList = new List<Shipping>();
                _importedList = new List<Shipping>();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Import()
        {
            try
            {
                if (MessageBox.Show("You have a " + _truckPackingList.Count() + " bales." + Environment.NewLine +
                    _truckPackingList.Where(x => x.ImportDate == null).Count() + " (new bales)" + Environment.NewLine +
                    _truckPackingList.Where(x => x.ImportDate != null).Count() + " (imported bales)" + Environment.NewLine +
                    "Do you want to do that?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                var updateList = _truckPackingList.Where(x => x.ImportDate != null).ToList();
                var addList = _truckPackingList.Where(x => x.ImportDate == null).ToList();
                // Update the old records.
                foreach (var item in updateList)
                {
                    var edit = BusinessLayerService.ShippingBLL()
                        .GetByBaleBarcode(item.BaleBarcode);

                    edit.Crop = item.Crop;
                    edit.FarmerID = item.FarmerID;
                    edit.DocumentNumber = item.DocumentNumber;
                    edit.BuyingDocumentCode = item.BuyingDocumentCode;
                    edit.ProjectTypeID = item.ProjectTypeID;
                    edit.RegisterBarcodeDate = item.RegisterBarcodeDate;
                    edit.BuyingGradeCrop = item.BuyingGradeCrop;
                    edit.BuyingGrade = item.BuyingGrade;
                    edit.BuyingGradeRecordDate = item.BuyingGradeRecordDate;
                    edit.BuyingWeight = item.BuyingWeight;
                    edit.BuyingWeightRecordDate = item.BuyingWeightRecordDate;
                    edit.LoadBaleToTruckDate = item.LoadBaleToTruckDate;
                    edit.UnitPrice = item.UnitPrice;
                    edit.TransportationCode = item.TransportationCode;
                    edit.TransportationTruckNo = item.TransportationTruckNo;
                    edit.ShippingCode = item.ShippingCode;
                    edit.ShippingToTruckDate = item.ShippingToTruckDate;

                    edit.ImportBy = sys_config.CurrentUser;
                    edit.ImportDate = DateTime.Now;

                    BusinessLayerService.ShippingBLL().Update(edit);
                }

                // Add a new records.
                foreach (var item in addList)
                {
                    item.ImportBy = sys_config.CurrentUser;
                    item.ImportDate = DateTime.Now;
                    BusinessLayerService.ShippingBLL().Add(item);
                }

                MessageBox.Show("Import and replace done!", "success", MessageBoxButton.OK, MessageBoxImage.Information);
                DataGridBinding();
                //foreach (var item in _truckPackingList)
                //{
                //    var row = _importedList.SingleOrDefault(t => t.BaleBarcode == item.BaleBarcode);
                //    if (row == null)
                //    {
                //        item.ImportBy = Environment.UserName;
                //        item.ImportDate = DateTime.Now;
                //        BusinessLayerService.ShippingBLL().Add(item);
                //    }
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                _truckPackingList = BusinessLayerService.ShippingBLL().ExcelTransform(filePathTextBox.Text);

                int totalDupplicate = 0;

                // เป็นการปรับค่าของ _list ที่ได้จากการอ่านข้อมูลจาก excel เพื่อนำไปแสดงบนหน้าจอว่า... ยาห่อใดบ้างที่เคยมีการ import ไปก่อนนี้แล้ว
                List<Shipping> dbList = new List<Shipping>();
                dbList = BusinessLayerService.ShippingBLL()
                    .GetByCrop(sys_config.DefaultCrop.Crop1);

                foreach (var item in _truckPackingList)
                    item.ImportBy = "New bale";

                if (dbList.Count > 0)
                {
                    foreach (var item in _truckPackingList)
                    {
                        var fromDB = dbList.SingleOrDefault(l => l.BaleBarcode == item.BaleBarcode);
                        if (fromDB != null && fromDB.ImportBy != null)
                        {
                            item.ImportBy = "Imported";
                            item.ImportDate = fromDB.ImportDate;
                            totalDupplicate++;
                        }
                    }
                }

                truckPackingListDataGrid.ItemsSource = null;
                truckPackingListDataGrid.ItemsSource = _truckPackingList.OrderBy(t => t.ImportBy);

                shippingCodeTextBox.Text = _truckPackingList.FirstOrDefault().ShippingCode;
                waitForImportTextBox.Text = string.Format("{0:N0}", _truckPackingList.Count - totalDupplicate);
                importdTextBox.Text = string.Format("{0:N0}", totalDupplicate);
                totalBalesTextBox.Text = string.Format("{0:N0}", _truckPackingList.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Import();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private System.Windows.Forms.OpenFileDialog InputFile(System.Windows.Forms.OpenFileDialog choofdlog,
            string Filter, int FilterIndex, bool Multiselect)
        {
            //Show file select dialog and get file name and location.
            choofdlog.Filter = Filter;
            choofdlog.FilterIndex = FilterIndex;
            choofdlog.Multiselect = Multiselect;
            return choofdlog;
        }

        private void browseFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog choofdlog = new System.Windows.Forms.OpenFileDialog();
                InputFile(choofdlog, "Excel Files|*.xls;*.xlsx;*.xlsm", 1, false);
                if (choofdlog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    filePathTextBox.Text = choofdlog.FileName;

                if (filePathTextBox.Text == "" || filePathTextBox.Text == null)
                    return;

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
