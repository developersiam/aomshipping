﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMShipping
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ContainerMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.SetupContainer());
        }

        private void ScanToContainerMenu_Click(object sender, RoutedEventArgs e)
        {
            Form.Shippings window = new Form.Shippings();
            window.ShowDialog();
        }

        private void ImportShippingDocumentMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.TransportationDocuments());
        }

        private void RePrintBarcodeMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.RePrintBarcodePage());
        }

        private void RPT001Menu_Click(object sender, RoutedEventArgs e)
        {
            Report.RPTSHP001 window = new Report.RPTSHP001();
            window.ShowDialog();
        }

        private void RPT002Menu_Click(object sender, RoutedEventArgs e)
        {
            Report.RPTSHP002 window = new Report.RPTSHP002(Guid.NewGuid());
            window.ShowDialog();
        }
    }
}
