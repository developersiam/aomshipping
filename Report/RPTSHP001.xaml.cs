﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMShipping.Report
{
    /// <summary>
    /// Interaction logic for RPTSHP004.xaml
    /// </summary>
    public partial class RPTSHP001 : Window
    {
        Crop _crop;
        public RPTSHP001()
        {
            InitializeComponent();

            _crop = new Crop();
            _crop = BusinessLayerService.CropBLL().GetDefaultCrop();

            PackingListByCropReportViewer.Reset();
            ReportDataSource ShippingDataSource = new ReportDataSource();
            ShippingDataSource.Value = Helper.ShippingHelper.GetByCrop(_crop.Crop1)
                .Where(x => x.ContainerID != null);
            ShippingDataSource.Name = "ShippingViewModelDataSet";
            PackingListByCropReportViewer.LocalReport.DataSources.Add(ShippingDataSource);
            PackingListByCropReportViewer.LocalReport.ReportEmbeddedResource = "AOMShipping.Report.RDLC.RPTSHP001.rdlc";
            PackingListByCropReportViewer.RefreshReport();
        }
    }
}
