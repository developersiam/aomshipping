﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMShipping.Report
{
    /// <summary>
    /// Interaction logic for RPTSHP003.xaml
    /// </summary>
    public partial class RPTSHP002 : Window
    {
        Guid _containerID;
        
        public RPTSHP002(Guid containerID)
        {
            try
            {
                InitializeComponent();
                _containerID = new Guid();
                _containerID = containerID;
                ContainerComboBoxBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ContainerComboBoxBinding()
        {
            try
            {
                ContainerComboBox.ItemsSource = null;
                ContainerComboBox.ItemsSource = BusinessLayerService.ContainerDetailBLL()
                    .GetByCrop(sys_config.DefaultCrop.Crop1)
                    .OrderBy(c => c.ContainerNo);

                ContainerComboBox.SelectedValue = _containerID;
                RenderReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RenderReport()
        {
            try
            {
                if (ContainerComboBox.SelectedValue == null)
                    return;

                PackingListByContainerReportViewer.Reset();
                ReportDataSource ShippingDataSource = new ReportDataSource();
                ShippingDataSource.Value = Helper.ShippingHelper.GetByContainerID(Guid.Parse(ContainerComboBox.SelectedValue.ToString()));
                ShippingDataSource.Name = "ShippingViewModelDataSet";
                PackingListByContainerReportViewer.LocalReport.DataSources.Add(ShippingDataSource);
                PackingListByContainerReportViewer.LocalReport.ReportEmbeddedResource = "AOMShipping.Report.RDLC.RPTSHP002.rdlc";
                PackingListByContainerReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ContainerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ContainerComboBox.SelectedIndex < 0)
                    return;

                RenderReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
