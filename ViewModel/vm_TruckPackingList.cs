﻿using System;

namespace AOMShipping.ViewModel
{
    public class vm_TruckPackingList
    {
        public short Crop { get; set; }
        public string ShippingCode { get; set; }
        public DateTime ShippingToTruckDate { get; set; }
        public double TotalWeight { get; set; }
        public double TotalBales { get; set; }
        public double TotalShipped { get; set; }
        public double TotalImported { get; set; }
    }
}
