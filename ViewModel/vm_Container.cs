﻿using AOMShipping.DomainModel;

namespace AOMShipping.ViewModel
{
    public class vm_Container : ContainerDetail
    {
        public int TotalBales { get; set; }
        public int TotalWeight { get; set; }
    }
}
