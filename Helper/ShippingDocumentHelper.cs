﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using AOMShipping.ViewModel;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping.Helper
{
    public static class ShippingDocumentHelper
    {
        public static vm_TruckPackingList GetByShippingCode(string shippingCode)
        {
            return BusinessLayerService.ShippingBLL().GetByShippingCode(shippingCode)
                .GroupBy(r => r.ShippingCode)
                .Select(t => new vm_TruckPackingList
                {
                    Crop = t.First().Crop,
                    ShippingCode = t.First().ShippingCode,
                    TotalBales = Convert.ToDouble(t.Count()),
                    TotalWeight = Convert.ToDouble(t.Sum(r => r.BuyingWeight)),
                    TotalShipped = Convert.ToDouble(t.Count(r => r.ToContainerDate != null)),
                    ShippingToTruckDate = t.First().ShippingToTruckDate.Date
                }).Single();
        }

        public static List<vm_TruckPackingList> GetByCrop(short crop)
        {
            return (BusinessLayerService.ShippingBLL().GetByCrop(crop)
                .GroupBy(r => r.ShippingCode)
                .Select(t => new vm_TruckPackingList
                {
                    Crop = t.First().Crop,
                    TotalBales = Convert.ToDouble(t.Count()),
                    TotalImported = Convert.ToDouble(t.Count()),
                    TotalWeight = Convert.ToDouble(t.Sum(r => r.BuyingWeight)),
                    TotalShipped = Convert.ToDouble(t.Where(r => r.ToContainerDate != null).Count()),
                    ShippingCode = t.First().ShippingCode,
                    ShippingToTruckDate = t.First().ShippingToTruckDate.Date
                })).ToList();
        }
    }
}
