﻿using AOMShipping.BLL;
using AOMShipping.DomainModel;
using AOMShipping.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping.Helper
{
    public static class ContainerDetailHelper
    {
        public static List<vm_Container> GetByCrop(short crop)
        {
            List<vm_Container> list1 = new List<vm_Container>();
            list1 = (BusinessLayerService.ShippingBLL().GetByCrop(crop)
                    .Where(s => s.ContainerID != null)
                    .GroupBy(s => s.ContainerID)
                    .Select(s => new vm_Container
                    {
                        Crop = s.First().Crop,
                        ContainerID = s.First().ContainerDetail.ContainerID,
                        ContainerNo = s.First().ContainerDetail.ContainerNo,
                        CreateDate = s.First().ContainerDetail.CreateDate,
                        CreateBy = s.First().ContainerDetail.CreateBy,
                        ModifiedBy = s.First().ContainerDetail.ModifiedBy,
                        ModifiedDate = s.First().ContainerDetail.ModifiedDate,
                        TotalBales = s.Count(),
                        TotalWeight = Convert.ToInt32(s.Sum(ss => ss.BuyingWeight))
                    })).ToList();

            List<ContainerDetail> list2 = new List<ContainerDetail>();
            list2 = BusinessLayerService.ContainerDetailBLL().GetByCrop(crop).ToList();

            if (list2.Count <= 0)
                return null;
            else
                return (from a in list2
                        join b in list1
                        on a.ContainerID equals b.ContainerID
                        into temp
                        from c in temp.DefaultIfEmpty()
                        select new vm_Container
                        {
                            Crop = a.Crop,
                            ContainerID = a.ContainerID,
                            ContainerNo = a.ContainerNo,
                            CreateDate = a.CreateDate,
                            CreateBy = a.CreateBy,
                            ModifiedBy = a.ModifiedBy,
                            ModifiedDate = a.ModifiedDate,
                            TotalBales = c == null ? 0 : c.TotalBales,
                            TotalWeight = c == null ? 0 : c.TotalWeight
                        }).ToList();
        }
    }
}
