﻿using AOMShipping.BLL;
using AOMShipping.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMShipping.Helper
{
    public static class ShippingHelper
    {
        public static List<vm_Shipping> GetByContainerID(Guid containerId)
        {
            return (BusinessLayerService.ShippingBLL().GetByContainerID(containerId)
                .Select(t => new vm_Shipping
                {
                    BaleBarcode = t.BaleBarcode,
                    BaleNumber = t.BaleNumber,
                    Crop = t.Crop,
                    FarmerID = t.FarmerID,
                    DocumentNumber = t.DocumentNumber,
                    BuyingDocumentCode = t.BuyingDocumentCode,
                    ProjectTypeID = t.ProjectTypeID,
                    RegisterBarcodeDate = t.RegisterBarcodeDate,
                    BuyingGradeCrop = t.BuyingGradeCrop,
                    BuyingGrade = t.BuyingGrade,
                    BuyingGradeRecordDate = t.BuyingGradeRecordDate,
                    BuyingWeight = t.BuyingWeight,
                    BuyingWeightRecordDate = t.BuyingWeightRecordDate,
                    LoadBaleToTruckDate = t.LoadBaleToTruckDate,
                    UnitPrice = t.UnitPrice,
                    TransportationCode = t.TransportationCode,
                    TransportationTruckNo = t.TransportationTruckNo,
                    ReceivedDate = t.ReceivedDate,
                    ReceivingWeight = t.ReceivingWeight,
                    ReceivedBy = t.ReceivedBy,
                    ShippingCode = t.ShippingCode,
                    ShippingToTruckDate = t.ShippingToTruckDate,
                    ImportBy = t.ImportBy,
                    ImportDate = t.ImportDate,
                    ContainerID = t.ContainerID,
                    ToContainerDate = t.ToContainerDate,
                    ToContainerUser = t.ToContainerUser,
                    ContainerNo = t.ContainerID == null ? null : t.ContainerDetail.ContainerNo
                })).ToList();
        }

        public static List<vm_Shipping> GetByCrop(short crop)
        {
            return (BusinessLayerService.ShippingBLL().GetByCrop(crop)
                .Select(t => new vm_Shipping
                {
                    BaleBarcode = t.BaleBarcode,
                    BaleNumber = t.BaleNumber,
                    Crop = t.Crop,
                    FarmerID = t.FarmerID,
                    DocumentNumber = t.DocumentNumber,
                    BuyingDocumentCode = t.BuyingDocumentCode,
                    ProjectTypeID = t.ProjectTypeID,
                    RegisterBarcodeDate = t.RegisterBarcodeDate,
                    BuyingGradeCrop = t.BuyingGradeCrop,
                    BuyingGrade = t.BuyingGrade,
                    BuyingGradeRecordDate = t.BuyingGradeRecordDate,
                    BuyingWeight = t.BuyingWeight,
                    BuyingWeightRecordDate = t.BuyingWeightRecordDate,
                    LoadBaleToTruckDate = t.LoadBaleToTruckDate,
                    UnitPrice = t.UnitPrice,
                    TransportationCode = t.TransportationCode,
                    TransportationTruckNo = t.TransportationTruckNo,
                    ReceivedDate = t.ReceivedDate,
                    ReceivingWeight = t.ReceivingWeight,
                    ReceivedBy = t.ReceivedBy,
                    ShippingCode = t.ShippingCode,
                    ShippingToTruckDate = t.ShippingToTruckDate,
                    ImportBy = t.ImportBy,
                    ImportDate = t.ImportDate,
                    ContainerID = t.ContainerID,
                    ToContainerDate = t.ToContainerDate,
                    ToContainerUser = t.ToContainerUser,
                    ContainerNo = t.ContainerID == null ? null : t.ContainerDetail.ContainerNo
                })).ToList();
        }
    }
}
